#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_STRING 15
const int BASE = 10;

typedef struct{
    char label[MAX_STRING];
    char dst[MAX_STRING], src[MAX_STRING], action[MAX_STRING];
    int caseNum;
} Case;

void swap(Case *case1, Case *case2){
    Case temp = *case1;
    *case1 = *case2;
    *case2 = temp;
}

void sort(Case *cases, int size){
    int i, j;
    for (i = 0; i < size - 3; i++){
        for (j = 0; j < size - i - 3; j++){
            if (cases[j].caseNum > cases[j + 1].caseNum){
                swap(&cases[j], &cases[j + 1]);
            }
        }
    }
}

void checkMinMax(int *min, int *max, int num){
    if (num > *max){
        *max = num;
    } else if (num < *min){
        *min = num;
    }
}

int isPointer(char *str){
    if (str[0] == '*'){
        return 1;
    }
    return 0;
}

int isNum(char *str){
    char *temp;
    if (strtol(str, &temp, BASE) != 0){
        return 1;
    }
    return 0;
}

char* toRegister(char* str){
    char *regStr = (char*) malloc(MAX_STRING * sizeof(char));
    if (regStr == NULL){
        printf("failed malloc regStr\n");
        exit(1);
    }

    if (strcmp(str, "result") == 0){
        strcpy(regStr, "%rax\0");
    } else if (strcmp(str, "*p1") == 0){
        strcpy(regStr, "(%rdi)\0");
    } else if (strcmp(str, "*p2") == 0){
        strcpy(regStr, "(%rsi)\0");
    }

    return regStr;
}

void printAction(Case *oneCase, FILE *writeFile, int isFirstAction){
    char *srcStr = toRegister(oneCase->src);
    char *dstStr = toRegister(oneCase->dst);

    if (isFirstAction){
        fprintf(writeFile, "%s:\n", oneCase->label);
    }

    if (isPointer(oneCase->src) && isPointer(oneCase->dst)){
        fprintf(writeFile, "movq %s, %%rbx\n", srcStr);

        if (strcmp(oneCase->action, "=") == 0){
            fprintf(writeFile, "movq %%rbx, %s\n", dstStr);
        } else if (strcmp(oneCase->action, "+=") == 0){
            fprintf(writeFile, "addq %%rbx, %s\n", dstStr);
        } else if (strcmp(oneCase->action, "-=") == 0){
            fprintf(writeFile, "subq %%rbx, %s\n", dstStr);
        } else if (strcmp(oneCase->action, "*=") == 0){
            fprintf(writeFile, "imulq %%rbx, %s\n", dstStr);
        } else if (strcmp(oneCase->action, "<<=") == 0){
            fprintf(writeFile, "movq %%rbx, %%rcx\n"
                               "shlq %%cl, %s\n", dstStr);
        } else if (strcmp(oneCase->action, ">>=") == 0){
            fprintf(writeFile, "movq %%rbx, %%rcx\n"
                               "sarq %%cl, %s\n", dstStr);
        }
    } else if (isNum(oneCase->src) == 0){
        if (strcmp(oneCase->action, "=") == 0){
            fprintf(writeFile, "movq %s, %s\n", srcStr, dstStr);
        } else if (strcmp(oneCase->action, "+=") == 0){
            fprintf(writeFile, "addq %s, %s\n", srcStr, dstStr);
        } else if (strcmp(oneCase->action, "-=") == 0){
            fprintf(writeFile, "subq %s, %s\n", srcStr, dstStr);
        } else if (strcmp(oneCase->action, "*=") == 0){
            fprintf(writeFile, "imulq %s, %s\n", srcStr, dstStr);
        } else if (strcmp(oneCase->action, "<<=") == 0){
            fprintf(writeFile, "movq %s, %%rcx\n"
                               "shlq %%cl, %s\n", srcStr, dstStr);
        } else if (strcmp(oneCase->action, ">>=") == 0){
            fprintf(writeFile, "movq %s, %%rcx\n"
                               "sarq %%cl, %s\n", srcStr, dstStr);
        }
    // if src is constant
    } else {
        if (strcmp(oneCase->action, "=") == 0){
            fprintf(writeFile, "movq $%s, %s\n", oneCase->src, dstStr);
        } else if (strcmp(oneCase->action, "+=") == 0){
            fprintf(writeFile, "addq $%s, %s\n", oneCase->src, dstStr);
        } else if (strcmp(oneCase->action, "-=") == 0){
            fprintf(writeFile, "subq $%s, %s\n", oneCase->src, dstStr);
        } else if (strcmp(oneCase->action, "*=") == 0){
            fprintf(writeFile, "imulq $%s, %s\n", oneCase->src, dstStr);
        } else if (strcmp(oneCase->action, "<<=") == 0){
            fprintf(writeFile, "shlq $%s, %s\n", oneCase->src, dstStr);
        } else if (strcmp(oneCase->action, ">>=") == 0){
            fprintf(writeFile, "sarq $%s, %s\n", oneCase->src, dstStr);
        }
    }

    free(srcStr);
    free(dstStr);
}

void parseCase(Case *cases, char** linesArr, int start, int end, FILE *writeFile){
    char *str;
    char *retLabel = ".L_RET";
    int caseIndex = 0;
    int isFirstAction, isBreak;
    int actionsCount;
    int i;
    for (i = start; i <= end; i++){
        str = strtok(linesArr[i], " ");
        if (strcmp(str, "case") == 0 || strcmp(linesArr[i], "default:\n") == 0){
            // update caseNum
            if (strcmp(str, "case") == 0){
                str = strtok(NULL, ":");
                cases[caseIndex].caseNum = atoi(str);
            }

            // update label
            if ( strcmp(linesArr[i], "default:\n") == 0){
                sprintf(cases[caseIndex].label, ".L_DEFAULT");
            } else {
                sprintf(cases[caseIndex].label, ".L%d", caseIndex + 1);
            }

            i++;
            actionsCount = 0;
            isBreak = 0;
            while (linesArr[i][0] == ' '){
                isFirstAction = 0;

                if (strstr(linesArr[i], "break;") != NULL){
                    fprintf(writeFile, "jmp %s\n", retLabel);
                    isBreak = 1;
                    break;
                }

                actionsCount++;
                if (actionsCount == 1){
                    isFirstAction = 1;
                }


                // update commands
                str = strtok(linesArr[i], " ");
                strcpy(cases[caseIndex].dst, str);
                str = strtok(NULL, " ");
                strcpy(cases[caseIndex].action, str);
                str = strtok(NULL, ";");
                strcpy(cases[caseIndex].src, str);

                printAction(&cases[caseIndex], writeFile, isFirstAction);
                i++;
            }

            // print label only if there no actions inside case
            if (actionsCount == 0 && strstr(linesArr[i], "case") != NULL){
                fprintf(writeFile, "%s:\n", cases[caseIndex].label);
                i--;
            } else if (isBreak == 0){
                i--;
            }

            caseIndex++;
        }
    }
}


int main(){

    // open switch.s
    FILE *writeFile;
    writeFile = fopen("switch.s", "w");
    if (writeFile == NULL){
        printf("ERROR!\n");
        exit(1);
    }

    fprintf(writeFile, "\t.section\t.text\n"
                       "\t.globl\tswitch2\n"
                       "switch2:\n");

    // open switch.c
    FILE *readFile;
    readFile = fopen("switch.c", "r");
    if (readFile == NULL){
        printf("ERROR! switch\n");
        exit(1);
    }

    char **linesArr = NULL, **tempLinesArr = NULL;
    char *line, *str;
    size_t len = 0;
    int lineLength;
    int countRows = 0;
    int numOfCases = 0;
    int firstCase = 0, defIndex = 0;
    int num;
    int max = 0, min = 0;

    // saving the file in array and updating information about cases
    while ((lineLength = getline(&line, &len, readFile)) != -1){

        if (countRows == 0){
            linesArr = (char**) malloc(sizeof(char*));
            if (linesArr == NULL){
                printf("failed malloc linesArr\n");
                exit(1);
            }
        } else {
            tempLinesArr = (char**) realloc(linesArr, (countRows + 1) * sizeof(char*));
            if (tempLinesArr == NULL){
                printf("failed reallocating tempLinesArr\n");
                exit(1);
            } else {
                linesArr = tempLinesArr;
                tempLinesArr = NULL;
            }
        }

        // allocate memory to each line according to lineLength plus room for null terminator
        countRows++;
        linesArr[countRows - 1] = (char*) malloc((lineLength + 1) * sizeof(char));
        if (linesArr[countRows - 1] == NULL){
            printf("failed malloc to line\n");
            exit(1);
        }
        strcpy(linesArr[countRows - 1], line);

        // updates min and max, and counts number of cases
        str = strtok(line, " ");
        if (strcmp(str, "case") == 0){
            str = strtok(NULL, ":");
            num = atoi(str);
            if (numOfCases == 0){
                min = max = num;
                firstCase = countRows - 1;
            }
            checkMinMax(&min, &max,num);
            numOfCases++;
        }

        if (strstr(line, "default:\n") != NULL){
            defIndex = countRows - 1;
            numOfCases++;
        }

    }

    fprintf(writeFile, "movq $0, %%rax\n"
                       "subq $%d, %%rdx\n"
                       "cmpq $%d, %%rdx\n"
                       "ja .L_DEFAULT\n"
                       "jmp *.L0(,%%rdx,8)\n" , min, max - min);

    Case *cases = (Case*) malloc(numOfCases * sizeof(Case));
    if (cases == NULL){
        printf("failed malloc cases\n");
        exit(1);
    }

    parseCase(cases, linesArr, firstCase, defIndex, writeFile);

    fprintf(writeFile, ".L_RET:\n"
                       "ret\n");

    fprintf(writeFile, "\t.section\t.rodata\n"
                       "\t.align\t8\n"
                       ".L0:\n");

    sort(cases, numOfCases);
    int i;
    int j = 0;
    for (i = 0; i < max - min + 1; i++){
        fprintf(writeFile, ".quad\t");
        if (i == cases[j].caseNum - min){
            fprintf(writeFile, "%s\n", cases[j++].label);
        } else {
            fprintf(writeFile, "%s\n", cases[numOfCases - 1].label);
        }
    }


    // TODO free
    for (i = 0; i < countRows; i++){
        free(linesArr[i]);
    }
    free(linesArr);
    free(cases);
    fclose(readFile);
    fclose(writeFile);

}
